﻿#include "MyApp.h"

#include <math.h>
#include <vector>

#include <array>
#include <list>
#include <tuple>

#include "imgui\imgui.h"

#include "ObjParser_OGL3.h"

CMyApp::CMyApp(void)
{
}


CMyApp::~CMyApp(void)
{
	
}

GLuint CMyApp::GenTexture()
{
	unsigned char tex[256][256][3];

	for (int i = 0; i<256; ++i)
		for (int j = 0; j<256; ++j)
		{
			tex[i][j][0] = 255.0f;
			tex[i][j][1] = 255.0f;
			tex[i][j][2] = 255.0f;
		}
	GLuint tmpID;

	// generáljunk egy textúra erõforrás nevet
	glGenTextures(1, &tmpID);
	// aktiváljuk a most generált nevû textúrát
	glBindTexture(GL_TEXTURE_2D, tmpID);
	// töltsük fel adatokkal az...
	glTexImage2D(GL_TEXTURE_2D,		// melyik binding point-on van a textúra erõforrás, amihez tárolást rendelünk
		0,					// melyik részletességi szint adatait határozzuk meg
		GL_RGB,				// textúra belsõ tárolási formátuma (GPU-n)
		//N, M,			// szélesség, magasság
		256, 256,			// szélesség, magasság
		0,					// nulla kell, hogy legyen ( https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage2D.xhtml )
		GL_RGB,				// forrás (=CPU-n) formátuma
		GL_UNSIGNED_BYTE,	// forrás egy pixelének egy csatornáját hogyan tároljuk
		tex);				// forráshoz pointer
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);	// bilineáris szûrés kicsinyítéskor
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);	// és nagyításkor is
	glBindTexture(GL_TEXTURE_2D, 0);

	return tmpID;
}

bool CMyApp::Init()
{


	// törlési szín legyen fehér
	glClearColor(1,1,1, 1);

	glEnable(GL_CULL_FACE); // kapcsoljuk be a hatrafele nezo lapok eldobasat
	glEnable(GL_DEPTH_TEST); // mélységi teszt bekapcsolása (takarás)
	//glDepthMask(GL_FALSE); // kikapcsolja a z-pufferbe történő írást - https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glDepthMask.xml

	//glEnable(GL_LINE_SMOOTH);

	// átlátszóság engedélyezése
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // meghatározza, hogy az átlátszó objektum az adott pixelben hogyan módosítsa a korábbi fragmentekből oda lerakott színt: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBlendFunc.xhtml

	
	m_program.Init({ 
		{ GL_VERTEX_SHADER, "myVert.vert"},
		{ GL_FRAGMENT_SHADER, "myFrag.frag"}
	},
	{ 
		{ 0, "vs_in_pos" },					// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 1, "vs_in_normal" },				// VAO 1-es csatorna menjen a vs_in_normal-ba
		{ 2, "vs_out_tex0" },				// VAO 2-es csatorna menjen a vs_in_tex0-ba
	});

	m_program.LinkProgram();

	

	std::vector<glm::vec3> _pos;
	_pos.resize((N + 1)*(M + 1));
	std::vector<glm::vec3> _nor;
	_nor.resize((N + 1)*(M + 1));
	std::vector<glm::vec2> _tex;
	_tex.resize((N + 1)*(M + 1));

	for (int i = 0; i <= N; ++i)
		for (int j = 0; j <= M; ++j)
		{
			float u = i / (float)N;
			float v = j / (float)M;

			_pos[i + j*(N + 1)] = glm::vec3(u, 0, v);
			_nor[i + j*(N + 1)] = glm::vec3(0, 1, 0);
			_tex[i + j*(N + 1)] = glm::vec2(u, v);
		}


	m_gpuBufferPos.BufferData(_pos);

	m_gpuBufferNormal.BufferData(_nor);

	// textúrakoordinátái
	m_gpuBufferTex.BufferData(_tex);

	// és a primitíveket alkotó csúcspontok indexei (az előző tömbökből) - triangle list-el való kirajzolásra felkészülve
	GLushort indices[3 * 2 * (N)*(M)];
	for (int i = 0; i<N; ++i)
		for (int j = 0; j<M; ++j)
		{
			
			//
			indices[6 * i + j * 3 * 2 * (N)+0] = (i)+(j)*	(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+1] = (i + 1) + (j)*	(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+2] = (i)+(j + 1)*(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+3] = (i + 1) + (j)*	(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+4] = (i + 1) + (j + 1)*(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+5] = (i)+(j + 1)*(N + 1);
		}
	// és a primitíveket alkotó csúcspontok indexei (az elõzõ tömbökbõl) - triangle list-el való kirajzolásra felkészülve
	m_gpuBufferIndices.BufferData(indices);

	// geometria VAO-ban való regisztrálása
	m_vao.Init(
		{
			{ CreateAttribute<0, glm::vec3, 0, sizeof(glm::vec3)>, m_gpuBufferPos },	// 0-ás attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_gpuBufferPos GPU pufferben vannak
			{ CreateAttribute<1, glm::vec3, 0, sizeof(glm::vec3)>, m_gpuBufferNormal },	// 1-es attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_gpuBufferNormal GPU pufferben vannak
			{ CreateAttribute<2, glm::vec2, 0, sizeof(glm::vec2)>, m_gpuBufferTex }
	},
		m_gpuBufferIndices
	);

	// textúra betöltése
	//m_textureMetal.FromFile("texture.png");

	// mesh betöltése
	//m_mesh = ObjParser::parse("suzanne.obj");

	// kamera
	m_camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);
	m_loc_texture = glGetUniformLocation(m_program, "texImage");
	m_textureID = GenTexture();

	return true;
}

void CMyApp::Clean()
{
	glDeleteTextures(1, &m_textureID);
}

void CMyApp::Update()
{

	m_camera.Update(0.0f);



}

void CMyApp::Render()
{
	// töröljük a frampuffert (GL_COLOR_BUFFER_BIT) és a mélységi Z puffert (GL_DEPTH_BUFFER_BIT)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	

	m_program.Use();

	
	

	m_program.SetUniform("eye_pos", m_camera.GetEye());
	//m_program.SetTexture("texImage", 0, m_textureMetal);
	m_program.SetTexture("texImage", 0, m_textureID);


	glm::mat4 surf =
		//glm::translate(Bezier(SDL_GetTicks() / 5000.0f)) *
		glm::rotate(3.14f, glm::vec3(0,0,1)) * 
		glm::scale(glm::vec3(15, 15, 15));
	m_program.SetUniform("world", surf);
	m_program.SetUniform("worldIT", glm::transpose(glm::inverse(surf)));
	m_program.SetUniform("MVP", m_camera.GetViewProj()*surf);
	m_program.SetUniform("Kd", glm::vec4(1, 1, 1, 1));


	m_program.SetUniform("uN", N);
	m_program.SetUniform("uM", M);

	m_vao.Bind();
	glDrawElements(GL_TRIANGLES, 3*2*(N)*(M), GL_UNSIGNED_SHORT, 0);
	m_vao.Unbind(); // nem feltétlen szükséges: a m_mesh->draw beállítja a saját VAO-ját


					// textúra kikapcsolása
	glBindTexture(GL_TEXTURE_2D, 0);
	
}

void CMyApp::KeyboardDown(SDL_KeyboardEvent& key)
{

}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key)
{
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse)
{
}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}

// a két paraméterbe az új ablakméret szélessége (_w) és magassága (_h) található
void CMyApp::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h );

	m_camera.Resize(_w, _h);
}


