#version 130

// VBO-b�l �rkez� v�ltoz�k
in vec3 vs_in_pos;
in vec3 vs_in_normal;
in vec2 vs_in_tex0;

// a pipeline-ban tov�bb adand� �rt�kek
out vec3 vs_out_pos;
out vec3 vs_out_normal;
out vec2 vs_out_tex0;
flat out int vs_out_i2;


// shader k�ls� param�terei - most a h�rom transzform�ci�s m�trixot k�l�n-k�l�n vessz�k �t
uniform mat4 world;
uniform mat4 worldIT;
uniform mat4 MVP;
uniform int uN;
uniform int uM;

vec2 cpx_mul(vec2 u, vec2 v){
	return vec2(u.x * v.x - u.y * v.y,  // val�s r�sz
				u.x * v.y + u.y * v.x); // k�pzetes r�sz
}

int mandelbrot(vec2 z)
{
vec2 c = z;
int i;
	for(i = 0; i < 500 && length(z) <= 2; ++i){
		z = cpx_mul(z,z) + c;
	}
	return i;
}

void main()
{
vec2 z = 4*vs_in_pos.xz-vec2(2,2);
int i2 = mandelbrot(z);
vec2 z3 = 4*(vs_in_pos.xz+vec2(1.0f/uN, 0))-vec2(2,2);
vec2 z4 = 4*(vs_in_pos.xz+vec2(0, 1.0f/uM))-vec2(2,2);
int i3 = mandelbrot(z3);
int i4 = mandelbrot(z4);
vec3 p = vec3(vs_in_pos.x, -i2/500.0f, vs_in_pos.z);
vec3 p3 = vec3(vs_in_pos.x+1.0f/uN, -i3/500.0f, vs_in_pos.z);
vec3 p4 = vec3(vs_in_pos.x, -i4/500.0f,vs_in_pos.z+1.0f/uM);
vec3 v1 = normalize(p3-p);
vec3 v2 = normalize(p4-p);


	gl_Position = MVP * (vec4( vs_in_pos, 1 )+ vec4(0,(-i2/500.f), 0, 0));

	vs_out_pos = (world *( vec4( vs_in_pos, 1 )+ vec4(0,(-i2/500.f), 0, 0))).xyz;
	//vs_out_pos = (world * (vec4( vs_in_pos, 1 )+ vec4(0,0,(i/500.0f),0))).xyz;
	//vs_out_pos = (world * vec4((i/500.0f),(i/500.0f),(i/500.0f),1)).xyz;
	vs_out_normal  = (worldIT * vec4(cross(v1,v2), 0)).xyz;
	vs_out_tex0 = vs_in_tex0;
	vs_out_i2=i3;
}