#pragma once

// C++ includes
#include <memory>

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "includes/gCamera.h"

#include "includes/ProgramObject.h"
#include "includes/BufferObject.h"
#include "includes/VertexArrayObject.h"
#include "includes/TextureObject.h"

// mesh
#include "includes/ObjParser_OGL3.h"

class CMyApp
{
public:
	CMyApp(void);
	~CMyApp(void);

	bool Init();
	void Clean();

	void Update();
	void Render();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);

protected:
	// shaderekhez sz�ks�ges v�ltoz�k
	ProgramObject		m_program;			// mesh shader
	ProgramObject		m_programSkybox;	// skybox shader
	ProgramObject		m_programAxes;		// axes shader

	// kocka
	VertexArrayObject	m_CubeVao;			// VAO
	IndexBuffer			m_CubeIndices;		// index buffer
	ArrayBuffer			m_CubeVertexBuffer;	// VBO
	void InitCube();
	// skybox kocka
	VertexArrayObject	m_SkyboxVao;
	IndexBuffer			m_SkyboxIndices;	
	ArrayBuffer			m_SkyboxPos;
	void InitSkyBox();
	// t�rusz
	VertexArrayObject	m_TorusVao;				// VAO
	IndexBuffer			m_TorusIndices;			// index buffer
	ArrayBuffer			m_TorusVertexBuffer;	// VBO
	void InitTorus();
	const int N = 100, M = 20;
	void createDisk();
	VertexArrayObject m_vao_disk;
	ArrayBuffer vbo_disk_pos;
	ArrayBuffer vbo_disk_norm;
	ArrayBuffer vbo_disk_tex;
	void drawDisk(const glm::mat4& world);


	// talaj
	VertexArrayObject	m_TalajVao;				// VAO
	IndexBuffer			m_TalajIndices;			// index buffer
	ArrayBuffer			m_TalajVertexBuffer;	// VBO
	void InitTalaj();

	// erd� h�tt�r
	VertexArrayObject	m_ErdoVao;				// VAO
	IndexBuffer			m_ErdoIndices;			// index buffer
	ArrayBuffer			m_ErdoVertexBuffer;		// VBO
	void InitErdo();

	// g�mb
	VertexArrayObject	m_GombVao;				// VAO
	IndexBuffer			m_GombIndices;			// index buffer
	ArrayBuffer			m_GombVertexBuffer;	// VBO
	void InitGomb();
	//henger
	VertexArrayObject	m_HengerVao;				// VAO
	IndexBuffer			m_HengerIndices;			// index buffer
	ArrayBuffer			m_HengerVertexBuffer;	// VBO
	void InitHenger();


	void DrawTree(const glm::mat4& trafo);
	void drawLeg(const glm::mat4& world);

	gCamera				m_camera;

	Texture2D			m_woodTexture;
	Texture2D			m_suzanneTexture;
	Texture2D			m_grassTexture;
	Texture2D			m_forestTexture;
	Texture2D			m_barkTexture;
	Texture2D			m_leavesTexture;
	Texture2D			m_earthTexture;
	Texture2D			m_shellTexture;
	Texture2D			m_scalesTexture;

	// nyers OGL azonos�t�k
	GLuint				m_skyboxTexture;

	struct Vertex
	{
		glm::vec3 p;
		glm::vec3 n;
		glm::vec2 t;
	};

	// mesh adatok
	Mesh *m_mesh_suzanne;
	Mesh* m_mesh_henger;

	// a jobb olvashat�s�g kedv��rt
	void InitShaders();

	struct Tree {
		glm::vec3 pos;
		float size;
		bool alive;
	};

	std::vector<std::vector<Tree>> m_trees;
	float m_treeGrowthTime = 10.0f; // mp
	float m_treeCutSize = 1.0f;
	int m_cutNum = 0; // kiv�gott f�k sz�ma

	glm::vec3 m_lightDir = -glm::vec3(1, 1, 1);

};

