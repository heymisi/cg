#version 140

// pipeline-ból bejövõ per-fragment attribútumok
in vec3 vs_out_pos;
in vec2 vs_out_tex0;

// kimenõ érték - a fragment színe
out vec4 fs_out_col;

uniform sampler2D texImage;
uniform int mode;

void main()
{
	fs_out_col = texture(texImage, vs_out_tex0);

	vec2 pos = vs_out_tex0 - 0.5;

	if(length(pos) > 0.5){
		if(mode == 0){
			discard;
		} else{
			fs_out_col = vec4(0,0,1,1);
		}
	}
}

// procedurális textúra...
