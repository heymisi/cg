﻿#include "MyApp.h"

#include <math.h>
#include <vector>
#include <random>

#include <array>
#include <list>
#include <tuple>
#include <imgui/imgui.h>
#include "includes/GLUtils.hpp"

CMyApp::CMyApp(void)
{
	m_camera.SetView(glm::vec3(5, 5, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	m_mesh = nullptr;
}

CMyApp::~CMyApp(void)
{
	std::cout << "dtor!\n";
}

glm::vec3 CMyApp::GetPos(float u, float v)
{
	// orig� k�z�ppont�, r sugar� g�mb parametrikus alakja: http://hu.wikipedia.org/wiki/G%C3%B6mb#Egyenletek 
	// figyelj�nk:	matematik�ban sokszor a Z tengely mutat felfel�, de n�lunk az Y, teh�t a legt�bb k�plethez k�pest n�lunk
	//				az Y �s Z koordin�t�k felcser�lve szerepelnek
	u *= float(2 * M_PI);
	v *= float(M_PI);
	float cu = cosf(u), su = sinf(u), cv = cosf(v), sv = sinf(v);
	float r = 1;

	return glm::vec3(r * cu * sv, r * cv, r * su * sv);
}
//
// egy parametrikus fel�let (u,v) param�ter�rt�kekhez tartoz� norm�lvektor�nak
// kisz�m�t�s�t v�gz� f�ggv�ny
//
glm::vec3 CMyApp::GetNorm(float u, float v)
{
	// K�plettel
	//u *= float(2 * M_PI);
	//v *= float(M_PI);
	//float cu = cosf(u), su = sinf(u), cv = cosf(v), sv = sinf(v);
	//return glm::vec3(cu*sv, cv, su*sv);

	// Numerikusan (nem kell ismerni a k�pletet, el�g a poz�ci��t)

	glm::vec3 du = GetPos(u + 0.01, v) - GetPos(u - 0.01, v);
	glm::vec3 dv = GetPos(u, v + 0.01) - GetPos(u, v - 0.01);

	return glm::normalize(glm::cross(du, dv));
}

glm::vec2 CMyApp::GetTex(float u, float v)
{
	return glm::vec2(1 - u, 1 - v);
}

void CMyApp::InitCube()
{
	//struct Vertex{ glm::vec3 position; glm::vec3 normals; glm::vec2 texture; };
	std::vector<Vertex>vertices;

	//front									 
	vertices.push_back({ glm::vec3(-0.5, -0.5, +0.5), glm::vec3(0, 0, -1), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(+0.5, -0.5, +0.5), glm::vec3(0, 0, -1), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(-0.5, +0.5, +0.5), glm::vec3(0, 0, -1), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(+0.5, +0.5, +0.5), glm::vec3(0, 0, -1), glm::vec2(1, 1) });
	//back
	vertices.push_back({ glm::vec3(+0.5, -0.5, -0.5), glm::vec3(0, 0, 1), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(-0.5, -0.5, -0.5), glm::vec3(0, 0, 1), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(+0.5, +0.5, -0.5), glm::vec3(0, 0, 1), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(-0.5, +0.5, -0.5), glm::vec3(0, 0, 1), glm::vec2(1, 1) });
	//right									 
	vertices.push_back({ glm::vec3(+0.5, -0.5, +0.5), glm::vec3(-1, 0, 0), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(+0.5, -0.5, -0.5), glm::vec3(-1, 0, 0), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(+0.5, +0.5, +0.5), glm::vec3(-1, 0, 0), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(+0.5, +0.5, -0.5), glm::vec3(-1, 0, 0), glm::vec2(1, 1) });
	//left									 
	vertices.push_back({ glm::vec3(-0.5, -0.5, -0.5), glm::vec3(1, 0, 0), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(-0.5, -0.5, +0.5), glm::vec3(1, 0, 0), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(-0.5, +0.5, -0.5), glm::vec3(1, 0, 0), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(-0.5, +0.5, +0.5), glm::vec3(1, 0, 0), glm::vec2(1, 1) });
	////top									 
	//vertices.push_back({ glm::vec3(-0.5, +0.5, +0.5), glm::vec3(0, 1, 0), glm::vec2(0, 0) });
	//vertices.push_back({ glm::vec3(+0.5, +0.5, +0.5), glm::vec3(0, 1, 0), glm::vec2(1, 0) });
	//vertices.push_back({ glm::vec3(-0.5, +0.5, -0.5), glm::vec3(0, 1, 0), glm::vec2(0, 1) });
	//vertices.push_back({ glm::vec3(+0.5, +0.5, -0.5), glm::vec3(0, 1, 0), glm::vec2(1, 1) });
	////bottom								 
	//vertices.push_back({ glm::vec3(-0.5, -0.5, -0.5), glm::vec3(0, -1, 0), glm::vec2(0, 0) });
	//vertices.push_back({ glm::vec3(+0.5, -0.5, -0.5), glm::vec3(0, -1, 0), glm::vec2(1, 0) });
	//vertices.push_back({ glm::vec3(-0.5, -0.5, +0.5), glm::vec3(0, -1, 0), glm::vec2(0, 1) });
	//vertices.push_back({ glm::vec3(+0.5, -0.5, +0.5), glm::vec3(0, -1, 0), glm::vec2(1, 1) });

	std::vector<int> indices(24);
	int index = 0;
	for (int i = 0; i < 4 * 4; i += 4)
	{
		indices[index + 0] = i + 0;
		indices[index + 1] = i + 1;
		indices[index + 2] = i + 2;
		indices[index + 3] = i + 1;
		indices[index + 4] = i + 3;
		indices[index + 5] = i + 2;
		index += 6;
	}

	//
	// geometria defini�l�sa (std::vector<...>) �s GPU pufferekbe val� felt�lt�se BufferData-val
	//

	// vertexek poz�ci�i:
	/*
	Az m_CubeVertexBuffer konstruktora m�r l�trehozott egy GPU puffer azonos�t�t �s a most k�vetkez� BufferData h�v�s ezt
	1. bind-olni fogja GL_ARRAY_BUFFER target-re (hiszen m_CubeVertexBuffer t�pusa ArrayBuffer) �s
	2. glBufferData seg�ts�g�vel �tt�lti a GPU-ra az argumentumban adott t�rol� �rt�keit

	*/

	m_CubeVertexBuffer.BufferData(vertices);

	// �s a primit�veket alkot� cs�cspontok indexei (az el�z� t�mb�kb�l) - triangle list-el val� kirajzol�sra felk�sz�lve
	m_CubeIndices.BufferData(indices);

	// geometria VAO-ban val� regisztr�l�sa
	m_CubeVao.Init(
		{
			// 0-�s attrib�tum "l�nyeg�ben" glm::vec3-ak sorozata �s az adatok az m_CubeVertexBuffer GPU pufferben vannak
			{ CreateAttribute<		0,						// attrib�tum: 0
									glm::vec3,				// CPU oldali adatt�pus amit a 0-�s attrib�tum meghat�roz�s�ra haszn�ltunk <- az elj�r�s a glm::vec3-b�l kik�vetkezteti, hogy 3 darab float-b�l �ll a 0-�s attrib�tum
									0,						// offset: az attrib�tum t�rol� elej�t�l vett offset-je, byte-ban
									sizeof(Vertex)			// stride: a k�vetkez� cs�cspont ezen attrib�tuma h�ny byte-ra van az aktu�list�l
								>, m_CubeVertexBuffer },
			{ CreateAttribute<1, glm::vec3, (sizeof(glm::vec3)), sizeof(Vertex)>, m_CubeVertexBuffer },
			{ CreateAttribute<2, glm::vec2, (2 * sizeof(glm::vec3)), sizeof(Vertex)>, m_CubeVertexBuffer },
		},
		m_CubeIndices
	);
}

void CMyApp::InitSkyBox()
{
	m_SkyboxPos.BufferData(
		std::vector<glm::vec3>{
		// back
		glm::vec3(-1, -1, -1),
			glm::vec3(1, -1, -1),
			glm::vec3(1, 1, -1),
			glm::vec3(-1, 1, -1),
			// front
			glm::vec3(-1, -1, 1),
			glm::vec3(1, -1, 1),
			glm::vec3(1, 1, 1),
			glm::vec3(-1, 1, 1),
	}
	);

	// A primitíveket alkotó csúcspontok indexei (az előző tömbökből) - triangle list-el való kirajzolásra felkészülve
	m_SkyboxIndices.BufferData(
		std::vector<int>{
		// back
		0, 1, 2,
			2, 3, 0,
			// front
			4, 6, 5,
			6, 4, 7,
			// left
			0, 3, 4,
			4, 3, 7,
			// right
			1, 5, 2,
			5, 6, 2,
			// bottom
			1, 0, 4,
			1, 4, 5,
			// top
			3, 2, 6,
			3, 6, 7,
	}
	);

	// geometria VAO-ban való regisztrálása
	m_SkyboxVao.Init(
		{
			{ CreateAttribute<0, glm::vec3, 0, sizeof(glm::vec3)>, m_SkyboxPos },
		}, m_SkyboxIndices
		);

	// skybox texture
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	glGenTextures(1, &m_skyboxTexture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	TextureFromFileAttach("assets/xpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	TextureFromFileAttach("assets/xneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	TextureFromFileAttach("assets/ypos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	TextureFromFileAttach("assets/yneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	TextureFromFileAttach("assets/zpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	TextureFromFileAttach("assets/zneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

}

void CMyApp::InitSquare()
{
	square_vbo.BufferData(
		std::vector<Vertex>{
			{ {-1, 0, 1}, { 0, 1, 0 }, { 0, 0 } },
			{ { 1, 0, 1}, {0, 1, 0}, {1, 0} },
			{ { 1, 0,-1}, {0, 1, 0}, {1, 1} },
			{ {-1, 0,-1}, {0, 1, 0}, {0, 1} },
	}
	);

	square_vao.Init(
		{
			// 0-�s attrib�tum "l�nyeg�ben" glm::vec3-ak sorozata �s az adatok az m_CubeVertexBuffer GPU pufferben vannak
			{ CreateAttribute<		0,						// attrib�tum: 0
									glm::vec3,				// CPU oldali adatt�pus amit a 0-�s attrib�tum meghat�roz�s�ra haszn�ltunk <- az elj�r�s a glm::vec3-b�l kik�vetkezteti, hogy 3 darab float-b�l �ll a 0-�s attrib�tum
									0,						// offset: az attrib�tum t�rol� elej�t�l vett offset-je, byte-ban
									sizeof(Vertex)			// stride: a k�vetkez� cs�cspont ezen attrib�tuma h�ny byte-ra van az aktu�list�l
								>, square_vbo },
			{ CreateAttribute<1, glm::vec3, (sizeof(glm::vec3)), sizeof(Vertex)>, square_vbo },
			{ CreateAttribute<2, glm::vec2, (2 * sizeof(glm::vec3)), sizeof(Vertex)>, square_vbo },
		}
	);
}

void CMyApp::InitSphere()
{
	std::vector<glm::vec3> pos;
	pos.reserve((N + 1) * (M + 1));
	std::vector<glm::vec3> norm;
	norm.reserve((N + 1) * (M + 1));
	std::vector<glm::vec2> tex;
	tex.reserve((N + 1) * (M + 1));

	for (int i = 0; i <= N; ++i)
		for (int j = 0; j <= M; ++j)
		{
			float u = i / (float)N;
			float v = j / (float)M;

			pos.push_back(GetPos(u, v));
			norm.push_back(GetNorm(u, v));
			tex.push_back(GetTex(u, v));
		}

	std::vector<int> ind;
	ind.reserve(3 * 2 * (N) * (M));
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j)
		{
			ind.push_back((i)+(j) * (N + 1));
			ind.push_back((i + 1) + (j) * (N + 1));
			ind.push_back((i)+(j + 1) * (N + 1));
			ind.push_back((i + 1) + (j) * (N + 1));
			ind.push_back((i + 1) + (j + 1) * (N + 1));
			ind.push_back((i)+(j + 1) * (N + 1));
		}

	sphere_pos.BufferData(pos);
	sphere_norm.BufferData(norm);
	sphere_tex.BufferData(tex);
	sphere_ind.BufferData(ind);

	sphere_vao.Init(
		{
			{ CreateAttribute<0, glm::vec3>, sphere_pos },
			{ CreateAttribute<1, glm::vec3>, sphere_norm },
			{ CreateAttribute<2, glm::vec2>, sphere_tex},
		},
		sphere_ind
		);
}

float get_plane_height(float pos_x, float pos_z) {
	return sin(pos_x) * cos(pos_z) / 4.0;
}

glm::vec3 CMyApp::get_plane_norm(int i, int j) {
	// Pozíciók parciális deriváltja i és j szerint

	glm::vec3 v1 = glm::normalize(glm::vec3(1, cos(i) * cos(j) / 4.0, 0));
	glm::vec3 v2 = glm::normalize(glm::vec3(0, -sin(i) * sin(j) / 4.0, 1));

	return -glm::normalize(glm::cross(v1, v2));
}

void CMyApp::InitPlane()
{
	std::vector<glm::vec3> pos;
	pos.reserve(plane_size * plane_size);
	std::vector<glm::vec3> norm;
	norm.reserve(plane_size * plane_size);
	std::vector<glm::vec2> tex;
	tex.reserve(plane_size * plane_size);

	for (int i = -10; i <= plane_size / 2; ++i)
	{
		for (int j = -10; j <= plane_size / 2; ++j)
		{
			pos.push_back(glm::vec3(i, get_plane_height(i, j), j));
			norm.push_back(get_plane_norm(i, j));
			tex.push_back(glm::vec2(i / (float)plane_size, j / (float)plane_size));
		}
	}

	std::vector<int> ind;
	ind.reserve(3 * 2 * plane_size * plane_size);
	for (int i = 0; i < plane_size; ++i)
		for (int j = 0; j < plane_size; ++j)
		{
			ind.push_back((i)+(j) * (plane_size + 1));
			ind.push_back((i + 1) + (j) * (plane_size + 1));
			ind.push_back((i)+(j + 1) * (plane_size + 1));
			ind.push_back((i + 1) + (j) * (plane_size + 1));
			ind.push_back((i + 1) + (j + 1) * (plane_size + 1));
			ind.push_back((i)+(j + 1) * (plane_size + 1));
		}

	plane_pos.BufferData(pos);
	plane_norm.BufferData(norm);
	plane_tex.BufferData(tex);
	plane_ind.BufferData(ind);

	plane_vao.Init(
		{
			{ CreateAttribute<0, glm::vec3>, plane_pos },
			{ CreateAttribute<1, glm::vec3>, plane_norm },
			{ CreateAttribute<2, glm::vec2>, plane_tex},
		},
		plane_ind
		);
}


void CMyApp::InitShaders()
{
	// a shadereket tároló program létrehozása az OpenGL-hez hasonló módon:
	m_program.AttachShaders({
		{ GL_VERTEX_SHADER, "myVert.vert"},
		{ GL_FRAGMENT_SHADER, "myFrag.frag"}
		});

	// attributomok osszerendelese a VAO es shader kozt
	m_program.BindAttribLocations({
		{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 1, "vs_in_norm" },			// VAO 1-es csatorna menjen a vs_in_norm-ba
		{ 2, "vs_in_tex" },				// VAO 2-es csatorna menjen a vs_in_tex-be
		});

	m_program.LinkProgram();

	// shader program rövid létrehozása, egyetlen függvényhívással a fenti három:
	m_programSkybox.Init(
		{
			{ GL_VERTEX_SHADER, "skybox.vert" },
			{ GL_FRAGMENT_SHADER, "skybox.frag" }
		},
		{
			{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		}
		);
}


void CMyApp::InitTrees()
{
	trees.resize(plane_size - 2);
	for (int i = 0; i < plane_size - 2; ++i) {

		trees[i].resize(plane_size - 2);

		for (int j = 0; j < plane_size - 2; ++j) {
			trees[i][j] = Tree{
				glm::vec3(i - 9, get_plane_height(i - 9, j - 9), j - 9), 0.5, false
			};
		}
	}

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> rnd(50, 100);
	std::uniform_int_distribution<> tree_pos(0, plane_size - 3);

	int tree_num = rnd(gen);

	for (int i = 0; i < tree_num; ++i)
	{
		int x = tree_pos(gen);
		int z = tree_pos(gen);
		if (trees[x][z].is_present) {
			--i;
		}
		else {
			trees[x][z].is_present = true;
		}
	}
}

bool CMyApp::Init()
{
	// törlési szín legyen kékes
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

	glEnable(GL_CULL_FACE); // kapcsoljuk be a hatrafele nezo lapok eldobasat
	glEnable(GL_DEPTH_TEST); // mélységi teszt bekapcsolása (takarás)

	InitShaders();
	InitCube();
	InitSkyBox();
	InitSquare();
	InitSphere();
	InitPlane();
	InitTrees();

	tree_spawn_time = SDL_GetTicks();


	leafTexture.FromFile("assets/leaves.jpg");
	barkTexture.FromFile("assets/bark.jpg");

	grassTexture.FromFile("assets/grass.jpg");
	forestTexture.FromFile("assets/forest.jpg");


	// mesh betoltese
	m_mesh = ObjParser::parse("assets/henger.obj");
	m_mesh->initBuffers();

	// kamera
	m_camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);

	return true;
}

void CMyApp::Clean()
{
	glDeleteTextures(1, &m_skyboxTexture);

	delete m_mesh;
}

void CMyApp::Update()
{
	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	season += delta_time / 20.0 * 2 * M_PI;

	m_camera.Update(delta_time);

	last_time = SDL_GetTicks();

	for (auto& tree_vec : trees) {
		for (auto& tree : tree_vec) {
			if (tree.is_present) {
				tree.scale += delta_time / 10.0;
				if (tree.scale > 1) {
					++num_of_trees_cut_down;
					tree.is_present = false;
				}
			}
		}
	}

	if (last_time > tree_spawn_time) {
		tree_spawn_time += 2000;

		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> rnd(0, 20);
		
		for (auto& tree_vec : trees) {
			for (auto& tree : tree_vec) {
				if (!tree.is_present) {
					if (rnd(gen) == 1) {
						tree.is_present = true;
						tree.scale = 0.05;
					}
				}
			}
		}
	}
}

void CMyApp::Render()
{
	// töröljük a frampuffert (GL_COLOR_BUFFER_BIT) és a mélységi Z puffert (GL_DEPTH_BUFFER_BIT)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (const auto& tree_vec : trees) {
		for (const auto& tree : tree_vec) {
			if (tree.is_present) {
				DrawTree(
					glm::translate(glm::vec3(tree.position)) *
					glm::scale(glm::vec3(tree.scale))
				);
			}
		}
	}

	DrawPlaneAndCube();
	DrawSkybox();
	DrawUI();
}


void CMyApp::DrawTree(glm::mat4& transform)
{
	glm::mat4 viewProj = m_camera.GetViewProj();

	sphere_vao.Bind();
	glm::mat4 trans =
		transform *
		glm::translate(glm::vec3(0, get_plane_height(0, 0) + 4, 0)) *
		glm::scale(glm::vec3(-1));

	m_program.Use();
	m_program.SetTexture("texImage", 0, leafTexture);
	m_program.SetUniform("MVP", viewProj * trans);
	m_program.SetUniform("world", trans);
	m_program.SetUniform("worldIT", glm::inverse(glm::transpose(trans)));
	m_program.SetUniform("season", season);
	m_program.SetUniform("light_dir", light_dir);
	m_program.SetUniform("eye_pos", m_camera.GetEye());

	glDrawElements(GL_TRIANGLES, 3 * 2 * N * M, GL_UNSIGNED_INT, nullptr);


	trans =
		transform *
		glm::translate(glm::vec3(0, get_plane_height(0, 0) + 2, 0));

	m_program.Use();
	m_program.SetTexture("texImage", 0, barkTexture);
	m_program.SetUniform("MVP", viewProj * trans);
	m_program.SetUniform("world", trans);
	m_program.SetUniform("worldIT", glm::inverse(glm::transpose(trans)));
	m_program.SetUniform("season", season);
	m_program.SetUniform("light_dir", light_dir);
	m_program.SetUniform("eye_pos", m_camera.GetEye());

	m_mesh->draw();
}

void CMyApp::DrawUI()
{
	if (ImGui::Begin("MyWindow")) {
		ImGui::SliderFloat3("Light dir", &light_dir[0], -5, 5);
		
		ImGui::Separator();
		
		ImGui::Text("Number of trees cut down: %i", num_of_trees_cut_down);
	}
	ImGui::End();

	
	//ImGui Testwindow
	ImGui::ShowTestWindow();
}

void CMyApp::DrawSkybox()
{
	// skybox
	GLint prevDepthFnc;
	glGetIntegerv(GL_DEPTH_FUNC, &prevDepthFnc);

	glDepthFunc(GL_LEQUAL);

	m_SkyboxVao.Bind();
	m_programSkybox.Use();
	m_programSkybox.SetUniform("MVP", m_camera.GetViewProj() * glm::translate(m_camera.GetEye()));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);
	glUniform1i(m_programSkybox.GetLocation("skyboxTexture"), 0);

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	m_programSkybox.Unuse();

	glDepthFunc(prevDepthFnc);
}

void CMyApp::DrawPlaneAndCube()
{
	glm::mat4 viewProj = m_camera.GetViewProj();

	// Plane

	plane_vao.Bind();

	glm::mat4 plane_trans = glm::translate(glm::vec3(0, 0, 0));

	m_program.Use();
	m_program.SetTexture("texImage", 0, grassTexture);
	m_program.SetUniform("MVP", viewProj * plane_trans);
	m_program.SetUniform("world", plane_trans);
	m_program.SetUniform("worldIT", glm::inverse(glm::transpose(plane_trans)));
	m_program.SetUniform("season", season);
	m_program.SetUniform("light_dir", light_dir);
	m_program.SetUniform("eye_pos", m_camera.GetEye());

	glDrawElements(GL_TRIANGLES, 3 * 2 * plane_size * plane_size, GL_UNSIGNED_INT, nullptr);


	// Cube

	m_CubeVao.Bind();

	glm::mat4 cube_trans =
		glm::translate(glm::vec3(0, 3, 0)) *
		glm::scale(glm::vec3(-20, -8, -20)) *
		glm::rotate<float>(M_PI, glm::vec3(1, 0, 0));

	m_program.Use();
	m_program.SetTexture("texImage", 0, forestTexture);
	m_program.SetUniform("MVP", viewProj * cube_trans);
	m_program.SetUniform("world", cube_trans);
	m_program.SetUniform("worldIT", glm::inverse(glm::transpose(cube_trans)));
	m_program.SetUniform("season", season);
	m_program.SetUniform("light_dir", light_dir);
	m_program.SetUniform("eye_pos", m_camera.GetEye());

	glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, nullptr);
}



void CMyApp::KeyboardDown(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardDown(key);
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardUp(key);
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse)
{
	m_camera.MouseMove(mouse);
}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}

void CMyApp::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h);

	m_camera.Resize(_w, _h);
}
