﻿#version 130

in vec3 vs_out_pos;
in vec3 vs_out_norm;
in vec2 vs_out_tex;

out vec4 fs_out_col;

uniform vec3 eye_pos = vec3(0, 15, 15);

uniform vec3 light_dir;

uniform vec4 La_summer = vec4(0.7,0.7,0.6, 1);
uniform vec4 Ld_summer = vec4(0.7,0.7,0.6, 1);
uniform vec4 Ls_summer = vec4(0.7,0.7,0.6, 1);

uniform vec4 La_winter = vec4(0.3,0.6,0.7, 1);
uniform vec4 Ld_winter = vec4(0.3,0.6,0.7, 1);
uniform vec4 Ls_winter = vec4(0.3,0.6,0.7, 1);

// anyagtulajdons�gok
uniform vec4 Ka = vec4(1, 1, 1, 0);
uniform vec4 Kd = vec4(0.5f, 0.5f, 0.5f, 1);
uniform vec4 Ks = vec4(1, 1, 1, 0);

uniform float specular_power = 64;
uniform float season;

uniform sampler2D texImage;

void main()
{
	float interpol_param = sin(season) / 2.0 + 0.5;
	vec4 La = mix(La_summer, La_winter, interpol_param);
	vec4 Ld = mix(Ld_summer, Ld_winter, interpol_param);
	vec4 Ls = mix(Ls_summer, Ls_winter, interpol_param);
	
	
	//*********************
	// ambient
	//*********************
	
	vec4 ambient = La * Ka;

	
	//*********************
	// diffuse
	//*********************
	
	vec3 normal = normalize( vs_out_norm );
	vec3 toLight = normalize( -light_dir );
	float di = clamp( dot( toLight, normal), 0.0f, 1.0f );
	vec4 diffuse = vec4(Ld.rgb*Kd.rgb*di, Kd.a);

	
	//*********************
	// specular
	//*********************

	vec4 specular = vec4(0);

	if ( di > 0 )
	{
		vec3 e = normalize( eye_pos - vs_out_pos );
		vec3 r = reflect( -toLight, normal );
		float si = pow( clamp( dot(e, r), 0.0f, 1.0f ), specular_power );
		specular = Ls*Ks*si;
	}

	fs_out_col = (ambient + diffuse + specular ) * texture(texImage, vs_out_tex.st);
}