﻿#pragma once

// C++ includes
#include <memory>

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "includes/gCamera.h"

#include "includes/ProgramObject.h"
#include "includes/BufferObject.h"
#include "includes/VertexArrayObject.h"
#include "includes/TextureObject.h"

// mesh
#include "includes/ObjParser_OGL3.h"

struct Tree {
	glm::vec3 position;
	float scale;
	bool is_present;
};

class CMyApp
{
public:
	CMyApp(void);
	~CMyApp(void);

	bool Init();
	void Clean();

	void Update();
	void Render();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);

	glm::vec3 GetPos(float u, float v);
	glm::vec3 GetNorm(float u, float v);
	glm::vec2 GetTex(float u, float v);

	glm::vec3 get_plane_norm(int i, int j);

protected:
	// shaderekhez sz�ks�ges v�ltoz�k
	ProgramObject		m_program;			// mesh shader
	ProgramObject		m_programSkybox;	// skybox shader

	VertexArrayObject	m_CubeVao;			// VAO
	IndexBuffer			m_CubeIndices;		// index buffer
	ArrayBuffer			m_CubeVertexBuffer;	// VBO
	VertexArrayObject	m_SkyboxVao;
	IndexBuffer			m_SkyboxIndices;
	ArrayBuffer			m_SkyboxPos;

	VertexArrayObject	square_vao;			// VAO
	ArrayBuffer			square_vbo;	// VBO


	VertexArrayObject	sphere_vao;
	IndexBuffer			sphere_ind;
	ArrayBuffer			sphere_pos;
	ArrayBuffer			sphere_norm;
	ArrayBuffer			sphere_tex;

	VertexArrayObject	plane_vao;
	ArrayBuffer			plane_pos;
	ArrayBuffer			plane_norm;
	ArrayBuffer			plane_tex;
	IndexBuffer			plane_ind;

	gCamera				m_camera;

	Texture2D			barkTexture;
	Texture2D			leafTexture;

	Texture2D			grassTexture;
	Texture2D			forestTexture;

	// nyers OGL azonos�t�k
	GLuint				m_skyboxTexture;

	struct Vertex
	{
		glm::vec3 p;
		glm::vec3 n;
		glm::vec2 t;
	};

	// mesh adatok
	Mesh* m_mesh;

	std::vector<std::vector<Tree>> trees;
	uint32_t tree_spawn_time;

	glm::vec3 light_dir = glm::vec3(-2, -1, -4);
	float season = 0;
	int num_of_trees_cut_down = 0;

	// a jobb olvashat�s�g kedv��rt
	void InitShaders();
	void InitCube();
	void InitSkyBox();
	void InitSquare();
	void InitSphere();
	void InitPlane();
	void InitTrees();

	void DrawTree(glm::mat4& transform);
	void DrawUI();
	void DrawSkybox();
	void DrawPlaneAndCube();

	const int plane_size = 20;

	const int N = 80;
	const int M = 40;
};

